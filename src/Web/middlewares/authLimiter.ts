import rateLimit from 'express-rate-limit';
import MongoStore from 'rate-limit-mongo';
import configuration from '../../Infrastructure/configuration';

const options = configuration.security.bruteSecurity;

export default rateLimit({
  store: new MongoStore({
    uri: `mongodb://${configuration.dataBase.host}/${configuration.dataBase.name}_security`,
    expireTimeMs: options.windowTime * 60 * 1000
  }),
  windowMs: options.windowTime * 60 * 1000,
  max: options.maxRequests,
  message: options.errorMessage
});
