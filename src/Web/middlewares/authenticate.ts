import { Request, Response, NextFunction } from 'express';
import expressJwt from 'express-jwt';
import configuration from '../../Infrastructure/configuration';

const checkToken = expressJwt({ secret: configuration.authentication.secret, algorithms: ['HS256'] });

const handleUnAuthError = (err: expressJwt.UnauthorizedError, _request: Request, response: Response, next: NextFunction) => {
  if (err.name === 'UnauthorizedError') {
    return response.status(401).send({ error: `Unauthorized: ${err.message}` });
  }
  return next();
};

export default [checkToken, handleUnAuthError];
