import { Request, Response, NextFunction } from 'express';
import { ServiceError } from '../../BLL/errors';

export const errorHandler = (err: ServiceError, _request: Request, response: Response, next: NextFunction) => {
  if (err.name === 'ServiceError') {
    return response.status(400).send({ error: err.message });
  }
  return next();
};
