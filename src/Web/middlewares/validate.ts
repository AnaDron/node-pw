import { Request, Response, NextFunction } from 'express';
import { validationResult, ValidationChain } from 'express-validator';

export default (validations: ValidationChain[]) => {
  return async (request: Request, response: Response, next: NextFunction) => {
    await Promise.all(validations.map((validation) => validation.run(request)));
    const errors = validationResult(request);
    if (errors.isEmpty()) {
      return next();
    }
    return response.status(400).json({ errors: errors.array() });
  };
};
