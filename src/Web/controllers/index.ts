import authRouter from './authController';
import accountRouter from './accountController';
import usersRouter from './usersController';
import transactionsRouter from './transactionsController';

export default [
  authRouter,
  accountRouter,
  usersRouter,
  transactionsRouter
];
