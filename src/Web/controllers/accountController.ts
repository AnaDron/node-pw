import { Router, Response } from 'express';
import { IUserRequest } from './util';
import { authenticate } from '../middlewares';
import { IGetBalanceResponse } from '../responses';
import { UsersService } from '../../BLL/services';

const usersService = new UsersService();

const path = 'account';

const router = Router();

router.get(
  `/${path}/balance`,
  authenticate,
  async (req: IUserRequest, res: Response) => {
    const balance = await usersService.getBalance(req.user.id);
    const result: IGetBalanceResponse = { balance };
    res.status(200).json(result);
  }
);

export default router;
