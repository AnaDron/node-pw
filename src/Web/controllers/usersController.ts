import { Router, Response } from 'express';
import { query } from 'express-validator';
import { IUserRequest } from './util';
import { authenticate, validate } from '../middlewares';
import { IGetOtherUsersRequests } from '../requests';
import { IGetOtherUsersResponse } from '../responses';
import { UsersService } from '../../BLL/services';

const usersService = new UsersService();

const path = 'users';

const router = Router();

router.get(
  `/${path}`,
  authenticate,
  validate([
    query('template').notEmpty(),
    query('limit').optional().isInt({ min: 1, max: 10 }).toInt()
  ]),
  async (req: IUserRequest, res: Response) => {
    const vm = req.query as unknown as IGetOtherUsersRequests;
    const users = await usersService.getOther(req.user.id, vm.template, vm.limit);
    const result: IGetOtherUsersResponse = { users };
    res.status(200).json(result);
  }
);

export default router;
