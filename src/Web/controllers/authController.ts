import { Router, Request, Response } from 'express';
import { body } from 'express-validator';
import { authLimiter, validate } from '../middlewares';
import { IRegistrationRequest, ILoginRequest } from '../requests';
import { ILoginResponse } from '../responses';
import { AuthService } from '../../BLL/services';

const authService = new AuthService();

const path = 'auth';

const router = Router();

router.post(
  `/${path}/register`,
  authLimiter,
  validate([
    body('name').notEmpty(),
    body('email').isEmail(),
    body('password').notEmpty(),
    body('passwordConfirm').custom((value, { req }) => {
      if (value !== req.body.password) {
        throw new Error('Password confirmation does not match password');
      }

      return true;
    })
  ]),
  async (req: Request, res: Response) => {
    const vm = req.body as IRegistrationRequest;
    await authService.register(vm.name, vm.email, vm.password);
    res.status(204).send();
  }
);

router.post(
  `/${path}/login`,
  authLimiter,
  validate([
    body('login').notEmpty(),
    body('password').notEmpty()
  ]),
  async (req: Request, res: Response) => {
    const vm = req.body as ILoginRequest;
    const token = await authService.login(vm.login, vm.password);
    const result: ILoginResponse = { token };
    res.status(200).json(result);
  }
);

export default router;
