import { Router, Response } from 'express';
import { query, body } from 'express-validator';
import { ObjectID } from 'mongodb';
import { IUserRequest } from './util';
import { authenticate, validate } from '../middlewares';
import { IGetTransactionsRequest, IAddTransactionRequest } from '../requests';
import { IGetTransactionsResponse } from '../responses';
import { TransactionsService } from '../../BLL/services';

const transactionsService = new TransactionsService();

const path = 'transactions';

const router = Router();

router.get(
  `/${path}`,
  authenticate,
  validate([
    query('limit').optional().isInt({ min: 1, max: 10 }).toInt()
  ]),
  async (req: IUserRequest, res: Response) => {
    const vm = req.query as IGetTransactionsRequest;
    const transactions = await transactionsService.getList(req.user.id, vm.limit);
    const result: IGetTransactionsResponse = { transactions };
    res.status(200).json(result);
  }
);

router.post(
  `/${path}`,
  authenticate,
  validate([
    body('contragentId').notEmpty().customSanitizer((value) => new ObjectID(value)),
    body('amount').isInt({ min: 1 }).toInt()
  ]),
  async (req: IUserRequest, res: Response) => {
    const vm = req.body as IAddTransactionRequest;
    await transactionsService.add(req.user.id, vm.contragentId.toString(), vm.amount);
    res.status(204).send();
  }
);

export default router;
