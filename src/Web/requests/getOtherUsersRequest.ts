export interface IGetOtherUsersRequests {
  template: string;
  limit?: number;
}
