import { ObjectID } from 'mongodb';

export interface IAddTransactionRequest {
  contragentId: ObjectID;
  amount: number;
}
