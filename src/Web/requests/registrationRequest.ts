export interface IRegistrationRequest {
  name: string;
  email: string;
  password: string;
  passwordConfirm: string;
}
