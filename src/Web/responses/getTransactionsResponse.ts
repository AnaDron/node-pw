import { ITransactionModel } from '../../Domain/models';

export interface IGetTransactionsResponse {
  transactions: ITransactionModel[];
}
