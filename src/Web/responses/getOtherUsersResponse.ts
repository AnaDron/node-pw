import { IUserSimpleModel } from '../../Domain/models';

export interface IGetOtherUsersResponse {
  users: IUserSimpleModel[];
}
