import {
  Document, Schema, model
} from 'mongoose';

const schema = new Schema({
  name: {
    type: String,
    required: true
  },
  login: {
    type: String,
    required: true,
    unique: true
  },
  passwordHash: {
    type: String,
    required: true
  },
  balance: {
    type: Number,
    required: true
  }
});

export interface IUserSimple extends Document {
  name: string;
}

export interface IUser extends IUserSimple {
  login: string;
  passwordHash: string;
  balance: number;
}

// eslint-disable-next-line @typescript-eslint/naming-convention
export const UserModel = model<IUser>('User', schema);
