import {
  Document, Schema, model
} from 'mongoose';
import { IUser, IUserSimple } from './user';

const schema = new Schema({
  payee: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  recipient: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  amount: {
    type: Number,
    required: true
  }
});

interface IBase extends Document {
  amount: number;
}

export interface ITransaction extends IBase {
  payee: IUser['_id'];
  recipient: IUser['_id'];
}

export interface ITransactionWithSimpleUsers extends IBase {
  payee: IUserSimple;
  recipient: IUserSimple;
}

// eslint-disable-next-line @typescript-eslint/naming-convention
export const TransactionModel = model<ITransaction>('Transaction', schema);
