import { IUserSimple } from '../user';
import { TransactionModel, ITransaction, ITransactionWithSimpleUsers } from '../transaction';
import { ITransactionCreateModel, ITransactionModel, IUserSimpleModel } from '../../../Domain/models';

export class TransactionRepository {
  public async add(model: ITransactionCreateModel): Promise<void> {
    await TransactionModel.create({
      payee: model.payeeId,
      recipient: model.recipientId,
      amount: model.amount
    });
  }

  public async getlist(userId: string, limit: number): Promise<ITransactionModel[]> {
    const list = (await TransactionModel
      .find({ $or: [{ payee: userId }, { recipient: userId }] })
      .limit(limit)
      .populate('payee', 'name')
      .populate('recipient', 'name')
      .exec()) as ITransactionWithSimpleUsers[];

    function mapUser(user: IUserSimple): IUserSimpleModel {
      return { id: user.id, name: user.name };
    }

    const result = list
      .map((x) => {
        return {
          id: x.id,
          payee: mapUser(x.payee),
          recipient: mapUser(x.recipient),
          amount: x.amount
        };
      });

    return result;
  }
}
