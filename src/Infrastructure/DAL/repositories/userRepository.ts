import { UserModel, IUserSimple } from '../user';
import { IUserCreateModel, IUserModel, IUserSimpleModel } from '../../../Domain/models';

export class UserRepository {
  public async add(model: IUserCreateModel): Promise<IUserModel> {
    const entity = await UserModel.create(model);
    return entity as IUserModel;
  }

  public async get(id: string): Promise<IUserModel | null> {
    const entity = await UserModel.findById(id);
    return entity as IUserModel;
  }

  public async find(conditions: Partial<IUserCreateModel>): Promise<IUserModel | null> {
    const entity = await UserModel.findOne(conditions);
    return entity as IUserModel;
  }

  public async getOther(id: string, template: string, limit: number): Promise<IUserSimpleModel[]> {
    const list = await UserModel
      .find({ _id: { $ne: id }, name: { $regex: template, $options: 'i' } })
      .limit(limit)
      .select('name')
      .exec() as IUserSimple[];

    const result = list
      .map((x) => { return { id: x.id, name: x.name }; });

    return result;
  }

  public async update(model: IUserModel): Promise<void> {
    await UserModel.findByIdAndUpdate(model.id, model);
  }
}
