import mongoose from 'mongoose';
import configuration from '../configuration';

export const dbInitialize = async () => {
  try {
    mongoose.set('useCreateIndex', true);
    await mongoose.connect(`mongodb://${configuration.dataBase.host}/${configuration.dataBase.name}`, { useNewUrlParser: true, useUnifiedTopology: true });
    console.log('MongoDB has started...');
  } catch (e) {
    console.log(`DB initialization error: ${e}`);
  }
};
