interface IDataBase {
    host: string;
    name: string;
}

interface IAuthentication {
    secret: string;
    saltRounds: number;
    tokenExpTime: string;
    passportSecret: string;
}

interface IServer {
    port: number;
}

interface IBruteSecurity {
    windowTime: number;
    maxRequests: number;
    errorMessage: string;
}

interface ISecurity {
    bruteSecurity: IBruteSecurity;
}

export default interface IConfiguration {
    dataBase: IDataBase;
    authentication: IAuthentication;
    server: IServer;
    security: ISecurity;
};
