import dotenv from 'dotenv';
import IConfiguration from './interface';

dotenv.config();

const configuration: IConfiguration = {
  dataBase: {
    host: process.env.dataBase_host,
    name: process.env.dataBase_name
  },
  authentication: {
    secret: 'secret',
    saltRounds: 10,
    tokenExpTime: '2h',
    passportSecret: 'passportSecret'
  },
  server: {
    port: parseInt(process.env.PORT, 10) || 5000
  },
  security: {
    bruteSecurity: {
      windowTime: parseInt(process.env.security_bruteSecurity_auth_windowTime, 10) || 1,
      maxRequests: parseInt(process.env.security_bruteSecurity_auth_maxRequests, 10) || 10,
      errorMessage: process.env.security_bruteSecurity_auth_errorMessage || 'Too many requests from this IP, please try again later'
    }
  }
};

export default configuration;
