export interface IUserCreateModel {
  name: string;
  login: string;
  passwordHash: string;
  balance: number;
}

export interface IUserModel extends IUserCreateModel {
  id: string;
}

export interface IUserSimpleModel {
  id: string;
  name: string;
}
