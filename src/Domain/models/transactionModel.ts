import { IUserSimpleModel } from './index';

export interface ITransactionCreateModel {
  payeeId: string;
  recipientId: string;
  amount: number;
}

export interface ITransactionModel {
  id: string;
  payee: IUserSimpleModel;
  recipient: IUserSimpleModel;
  amount: number;
}
