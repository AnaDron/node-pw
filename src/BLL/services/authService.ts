import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import { ServiceError } from '../errors';
import configuration from '../../Infrastructure/configuration';
import { UserRepository } from '../../Infrastructure/DAL/repositories';

export class AuthService {
  private userRepository = new UserRepository();

  public async register(name: string, email: string, password: string): Promise<void> {
    const user = await this.userRepository.find({ login: email });
    if (user) throw new ServiceError('User exist');

    const hash = await bcrypt.hash(password, configuration.authentication.saltRounds);

    await this.userRepository.add({
      login: email,
      name,
      passwordHash: hash,
      balance: 500
    });

    console.log(`User ${name} registered!`);
  }

  public async login(login: string, password: string): Promise<string> {
    const user = await this.userRepository.find({ login });
    if (!user) throw new ServiceError('Invalid login or password');

    const compareResult = await bcrypt.compare(password, user.passwordHash);
    if (!compareResult) throw new ServiceError('Invalid login or password');

    const tokenPayload = { id: user.id };

    const token = jwt.sign(tokenPayload, configuration.authentication.secret, {
      algorithm: 'HS256',
      expiresIn: configuration.authentication.tokenExpTime
    });

    return token;
  }
}
