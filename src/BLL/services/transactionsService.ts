import { ServiceError } from '../errors';
import { ITransactionModel } from '../../Domain/models';
import { UserRepository, TransactionRepository } from '../../Infrastructure/DAL/repositories';

export class TransactionsService {
  private userRepository = new UserRepository();

  private transactionRepository = new TransactionRepository();

  public async add(payeeId: string, recipientId: string, amount: number): Promise<void> {
    if (recipientId === payeeId) throw new ServiceError('Recepient and payee must not be the same');

    const payee = await this.userRepository.get(payeeId);
    if (!payee) throw new ServiceError('Payee not found');

    if (payee.balance < amount) throw new ServiceError('Balance of payee less then amount');

    const recipient = await this.userRepository.get(recipientId);
    if (!recipient) throw new ServiceError('Recipient not found');

    payee.balance -= amount;
    await this.userRepository.update(payee);

    await this.transactionRepository.add({
      payeeId,
      recipientId,
      amount
    });

    recipient.balance += amount;
    await this.userRepository.update(recipient);
  }

  public async getList(userId: string, limit: number = 5): Promise<ITransactionModel[]> {
    const user = await this.userRepository.get(userId);
    if (!user) throw new ServiceError('User not found');

    const result = await this.transactionRepository.getlist(userId, limit);

    return result;
  }
}
