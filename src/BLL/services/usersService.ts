import { ServiceError } from '../errors';
import { IUserSimpleModel } from '../../Domain/models';
import { UserRepository } from '../../Infrastructure/DAL/repositories';

export class UsersService {
  private userRepository = new UserRepository();

  public async getBalance(userId: string): Promise<number> {
    const user = await this.userRepository.get(userId);
    if (!user) throw new ServiceError('User not found');

    return user.balance;
  }

  public async getOther(userId: string, template: string, limit: number = 5): Promise<IUserSimpleModel[]> {
    const user = await this.userRepository.get(userId);
    if (!user) throw new ServiceError('User not found');

    const result = await this.userRepository.getOther(userId, template, limit);

    return result;
  }
}
