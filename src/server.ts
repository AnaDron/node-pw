import express from 'express';
import 'express-async-errors';
import morgan from 'morgan';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import configuration from './Infrastructure/configuration';
import { dbInitialize } from './Infrastructure/DAL';
import router from './Web/controllers';
import { errorHandler } from './Web/middlewares/errorHandler';

dbInitialize();

const app = express();

app.use(helmet());
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
// app.use(cors({
//     origin: (origin, callback) => {
//         callback(null, true);
//     },
//     credentials: true
// }));

app.use('/api', router);

app.use(errorHandler);

app.use((err, req, res, next) => {
  if (res.headersSent) {
    next(err);
    return;
  }
  res.status(err.status || 500);
  console.log(`Internal error(${res.statusCode}): ${err.message}`);
  res.send({ error: err.message });
});

app.use((req, res, next) => {
  res.status(404);
  console.log(`Not found URL: ${req.url}`);
  res.send({ error: 'Not found' });
});

app.listen(configuration.server.port, () => {
  console.log(`Express server listening on port ${configuration.server.port}`);
});
